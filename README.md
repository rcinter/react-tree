# floweditor


```js
import React from 'react'
import FlowEditor from 'floweditor'

const Editor = () => (
  <FlowEditor data={[{
                      "_id": {
                        "$oid": "5d5517e6b9e9b2b334044995"
                      },
                      "action_id": "open account",
                      "script": "flow",
                      "wait_for_input": true,
                      "text": [
                        "You can open an account. ЩЉЩ…ЩѓЩ†Щѓ ЩЃШЄШ­ Ш­ШіШ§ШЁ."
                      ],
                      "intent": "open account",
                      "delay": "0",
                      "button": [],
                      "quick": [],
                      "intro": false,
                      "outro": false
                    },
                    {
                      "_id": {
                        "$oid": "5d5517e6b9e9b2b33404489e"
                      },
                      "action_id": "when do payments arrive",
                      "script": "main",
                      "wait_for_input": true,
                      "text": [
                        "Pleace choose action:\n1. Open account\n2. Close account\n3.Whatever"
                      ],
                      "intent": "when do payments arrive",
                      "button": [],
                      "quick": [
                        "1",
                        "2"
                      ],
                      "intro": false,
                      "outro": false,
                      "router": {
                        "\\u002e*1": "flow.open account",
                        "\\u002e*2": "main.GoodBye",
                      }
                    },
                    {
                      "_id": {
                        "$oid": "5d5517e6b9e9b2b334044886"
                      },
                      "action_id": "GoodBye",
                      "script": "main",
                      "wait_for_input": false,
                      "text": [
                        "Bye! рџ‘‹ рџ‘‹ рџ‘‹",
                        ""
                      ],
                      "intent": "GoodBye",
                      "delay": "0",
                      "button": [],
                      "quick": [],
                      "intro": false,
                      "outro": false
                    }]} />
)
```

# flowtree


```js
import React from 'react'
import FlowTree from 'flowtree'

const Tree = () => (
  <FlowTree data={[
    { name: 'file 1' },
    { name: 'file 2' },
    { name: 'file 3', 
      childNodes: [
        { name: 'file 1-1' },
        { name: 'file 1-2' },
        { name: 'file 1-3' , 
            childNodes: [
              { name: 'file 1-3-1' },
              { name: 'file 1-3-2', 
                childNodes: [
                  { name: 'file 1-2-3-1' },
                ]},
              { name: 'file 1-3-2-3' },
            ]},
      ] 
    }
  ]} />
)
```

Callbacks usage

```js
const onSelect = (node) => {
  console.log(`node ${node.path} selected`)
}

const onDeselect = (node) => {
  console.log(`node ${node.path} deselected`)
}

const onOpen = (node) => {
  console.log(`node ${node.path} opened`)
}

const onClose = (node) => {
  console.log(`node ${node.path} close`)
}

<FlowTree onSelect={onSelect}
          onDeselect={onDeselect}  
          onOpen={onOpen}
          onClose={onClose}
          data={[
            // ...
          ]} />
```

> \* A node is an actual instance of its belonging React.Component.

To try, use `yarn`:

    $ yarn
    $ yarn start

