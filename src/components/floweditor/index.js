import PropTypes from 'prop-types'
import React from 'react'
import FlowTree from '../flowtree'
import augumentData from './utils'
import './style.css'

class FlowEditor extends React.Component {
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.object),
    mode: PropTypes.string,
    onSelectChange: PropTypes.func,
    onOpenClose: PropTypes.func,
  }

  static defaultProps = {
    data: () => [{}],
    mode: 'bot',
    onSelectChange: () => {},
    onOpenClose: () => {}
  }
  
  get class() {
    return `(${this.constructor.name})`
  }

  constructor(props) {
    super(props)
    
    
    var treeData, botMap
    if(props.mode === 'bot'){
      [treeData, botMap] = augumentData(props.data)
    }else {
      treeData = props.data
      botMap = []
    }
    
    this.botMap = botMap

    this.state = {
        treeData: treeData,
        debugJson: {},
    }
  }

  onSelectChange = (selectedNodes) => {
      this.setState({debugJson: this._findPayloads(selectedNodes)})
      this.props.onSelectChange(selectedNodes)
  }

  _findPayloads = (nodes) => {
    if(!(nodes && nodes.length)) return
    var json = {}
    if(this.botMap) nodes.forEach(node => {
      console.log(this.class, `bot payload [${node.path}]: `, this.botMap[node.path])
      if(node) json[node.name] = this.botMap[node.path]
    }) 
    return json
  }

  render() {
    return (<div className="row">
                <div className="col-6 flowTree">
                    <FlowTree onSelect={this.onSelectChange}
                              onDeselect={this.onSelectChange}  
                              onOpen={this.props.onOpenClose}
                              onClose={this.props.onOpenClose}
                              data={this.state.treeData} />
                </div>
                <div className="col-6 flowTreeLog"> 
                    <pre>
                        {JSON.stringify(this.state.debugJson, null, 1)}
                    </pre>
                </div>
            </div>
    )
  }
}

export default FlowEditor
