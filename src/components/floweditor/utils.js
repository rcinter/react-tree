const alphabeticComparator = (a, b) => {
  return (a.name[0].toLowerCase()).charCodeAt(0) - (b.name[0].toLowerCase()).charCodeAt(0);
}

const unicodeToChar = (text) => {
   return text.replace(/\\u[\dA-F]{4}/gi, 
          function (match) {
               return String.fromCharCode(parseInt(match.replace(/\\u/g, ''), 16));
          });
}

const convertRoute2Path = (route) => {
   const path = "~/" + route.replace(".", "/");
   //console.log(`Converting: [${route}] => [${path}]`)
   return path
}

const augumentData = (dataset) => {
    var nodeMap = {}          // temporary map
    var flatMapPayloads = {}  // raw flat bot action map

    // Parse input to nodeMap
    dataset.forEach(function(element) {
      /** Fill raw element into nodeMap */
      if(!nodeMap[element.script]) nodeMap[element.script] = {}
      if(!nodeMap[element.script][element.action_id]) 
        nodeMap[element.script][element.action_id] = {}
      nodeMap[element.script][element.action_id] = element

      /** Fill bot data map */
      if(element.script){
        // Fill branch path
        if(!flatMapPayloads[convertRoute2Path(element.script)]) 
          flatMapPayloads[convertRoute2Path(element.script)] = []
        flatMapPayloads[convertRoute2Path(element.script)].push(element)
        // Fill node pat
        flatMapPayloads[convertRoute2Path(element.script + "." + element.action_id)] = element
      }
    });
    
    // Prepare tree nodes
    var treeNodes = []
    for (var scriptName in nodeMap) {
      if (Object.prototype.hasOwnProperty.call(nodeMap, scriptName)){    
        var childNodes = []
        const scriptActions = nodeMap[scriptName]
        for (var actionName in scriptActions) {
            const action = scriptActions[actionName]
            const router = action.router;
            var linkNodes = []
            if(router)
              for (var condition in router) {
                if (Object.prototype.hasOwnProperty.call(router, condition)){
                  var [nodeScript, nodeAction] = router[condition].split('.');
                  linkNodes.push({name: unicodeToChar(condition), 
                                  linkTo: { 
                                            path: convertRoute2Path(router[condition]),
                                            name: nodeAction
                                          }
                                  })
                }
              }
            childNodes.push({name: actionName, childNodes: linkNodes.sort(alphabeticComparator)})                     
        }
        treeNodes.push({name: scriptName, childNodes: childNodes.sort(alphabeticComparator)})
      }
    }

    return [treeNodes.sort(alphabeticComparator), flatMapPayloads];
}

export default augumentData