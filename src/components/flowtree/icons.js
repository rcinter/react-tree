import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFolderPlus,
         faFolderMinus,
         faFolder,
         faComment,
         faComments,
         faLevelUpAlt,
         faPlus,
         faMinus,
         faExpandArrowsAlt,
         faCompressArrowsAlt,
         faLongArrowAltRight
         } from '@fortawesome/free-solid-svg-icons'

const CaretClosed = props => (
  <FontAwesomeIcon {...props} className="tree-icon" icon={faPlus}/>
)
const CaretOpened = props => (
  <FontAwesomeIcon {...props} className="tree-icon" icon={faMinus}/>
)
const ScriptOpened = (props) => (
  <FontAwesomeIcon {...props} className="tree-icon" icon={faFolderMinus}/>
)
const ScriptClosed = (props) => (
  <FontAwesomeIcon {...props} className="tree-icon" icon={faFolderPlus}/>
)
const ScriptEmpty = (props) => (
  <FontAwesomeIcon {...props} className="tree-icon" icon={faFolder}/>
)
const Node = (props) => (
  <FontAwesomeIcon {...props} className="tree-icon" icon={faComment}/>
)
const Nodes = (props) => (
  <FontAwesomeIcon {...props} className="tree-icon" icon={faComments}/>
)
const LinkArrow = (props) => (
  <FontAwesomeIcon {...props} className="tree-icon" icon={faLevelUpAlt} rotation={90}/>
)
const LongArrow = (props) => (
  <FontAwesomeIcon {...props} className="tree-icon" icon={faLongArrowAltRight}/>
)
const Expand = (props) => (
  <FontAwesomeIcon {...props} icon={faExpandArrowsAlt}/>
)
const Collapse = (props) => (
  <FontAwesomeIcon {...props} icon={faCompressArrowsAlt}/>
)

const Plus = (props) => (
  <FontAwesomeIcon {...props} icon={faPlus}/>
)


const getIcon = ({empty = true, opened = false, type = 'node', depth = 1}) => {
  if(depth === 1)
    return !empty?opened?<ScriptOpened />:<ScriptClosed />:<ScriptEmpty />
  else if(type === 'node') return empty?<Node/>:<Nodes/>
  else return <LinkArrow />
}

const getCaret = (opened) => {
    return opened ? (
        <CaretOpened />
    ) : (
        <CaretClosed />
    )
}

export default {
  LongArrow,
  Expand,
  Collapse,
  Plus,
  getIcon,
  getCaret
}
