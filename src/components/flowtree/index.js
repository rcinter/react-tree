import PropTypes from 'prop-types'
import React from 'react'

import './FlowBranch'
import './FlowNode'
import './style.css'
import { exports } from './modules'
import Icons from './icons'

export const FlowBranch = exports.FlowBranch
export const FlowNode = exports.FlowNode

class PathCrumbs extends React.Component {
  static propTypes = {
    path: PropTypes.string.isRequired
  }
  static defaultProps = {
  path: '~'
  }
  render() {
    const crumbs = this.props.path && this.props.path.split('/').map((breadcrumb, index) => {
      return (<li key={`breadcrumb_${index}`} className="breadcrumb-item">{breadcrumb}</li>)
    })
    return crumbs?<nav aria-label="breadcrumb" className="position-sticky">
            <div className="breadcrumb pathcrumb">
              {crumbs}  
            </div>
            
          </nav>:''
  }
}

class TreeHeader extends React.Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    children: PropTypes.arrayOf(PropTypes.object),
    path: PropTypes.string
  }
  render() {
    const buttons = this.props.children && this.props && this.props.children.map((child, index) => {
      return (<li key={`navbtn_${index}`} className="nav-item ml-1">{child}</li>)
    })
    return <div className="sticky-top">
      <nav className="flowTreeHeader navbar navbar-expand-lg navbar-light bg-light">
        <div className="navbar-brand">
          <h6>{this.props.title}</h6>
        </div>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav ml-auto">
            {buttons}
          </ul>
        </div>
      </nav>
      <PathCrumbs path={this.props.path}/>
    </div>
  }
}

const convertPath2Route = (path) => {
   const route = path.replace("~/", "").replace(/\//g, ".");//.replace("/",".");
   console.log(`Converting: [${path}] => [${route}]`)
   return route
}


export class FlowTree extends React.PureComponent {
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.object),
    className: PropTypes.string,
    onSelect: PropTypes.func,
    onDeselect: PropTypes.func,
    onClose: PropTypes.func,
    onOpen: PropTypes.func
  }

  constructor(props) {
    super(props)
    this._path = '~'
    this._childNodes = []
    this._selectedNodePaths = []
    this._openedNodes = []
    this._treeMap = {}

    this.state = { data: props.data }
    
  }

  static defaultProps = {
    onSelect: () => {},
    onDeselect: () => {},
    onClose: () => {},
    onOpen: () => {},
  }

  get class() {
    return `(${this.constructor.name})`
  }

  get childNodes() {
    return this._childNodes
  }

  get path() {
    return this._path
  }

  get selectedCount() {
    //return countProps(this._selectedNodePaths)
    return this._selectedNodePaths.length || 0
  }
  get openedCount() {
    //return countProps(this._openedNodes)
    return this._openedNodes.length || 0
  }
  
  get selectedNodes() {
    return this._selectedNodePaths.map(path => {
       return this._treeMap[path]
    })
  }


  getSnapshotBeforeUpdate() {
    this._childNodes = []
    return null
  }

  componentDidUpdate() {}

  handleSelect = (node) => {
    console.log(this.class, `handleSelect: [${node.path}]  depth: ${node.depth} hidden: ${node.hidden} ...`)        
    this._clearSelectedNodes()
    const context = this

    console.log(node.linkTo)

    if(node.linkTo){
      const linkedNode = this._treeMap[node.linkTo.path]

      

      if(linkedNode){
        context._openBranchUp(linkedNode)
        linkedNode.select()
      }
    }
    
    this._selectedNodePaths.push(node.path)
    
    this._refreshComponent()  
    this.props.onSelect(this.selectedNodes)   
  }

  handleDeselect = (node) => {
    console.log(this.class, `handleDeselect: [${node.path}]  depth: ${node.depth} hidden: ${node.hidden} ...`)        
    if(node.linkTo){
      const linkedNode = this._treeMap[node.linkTo]
      if(linkedNode) linkedNode.deselect()
    }
    // Remove deselected node
    this._selectedNodePaths = this._selectedNodePaths.filter(path => {
      return node.path !== path
    });  

    this._refreshComponent()
    this.props.onDeselect(this.selectedNodes)   
  }
  handleOpen = (node) => {
    console.log(this.class, "handleOpen: ", node, node.depth)        
    this._openedNodes.push(node.path)

    this._refreshComponent()
    this.props.onOpen(node)
  }

  handleClose = (node) => {
    console.log(this.class, "handleClose: ", node, node.depth)   
    this._openedNodes = this._openedNodes.filter(path => {
      return node.path !== path
    });  
    
    this._refreshComponent()    
    this.props.onClose(node)
  }

  clearSelection = evt => {
    evt.preventDefault()
    this._clearSelectedNodes()
    this._refreshComponent()
  }

  collapseTree = evt => {
    evt.preventDefault()
    this._collapseTreeNodes()    
    this._refreshComponent()
  }

  expandTree = evt => {
    evt.preventDefault()
    this._expandTreeNodes()    
    this._refreshComponent()
  }

  _clearSelectedNodes = () => {
    console.log(this.class, `_clearSelectedNodes: ${this._selectedNodePaths.length} ...`)
    while(this._selectedNodePaths.length){
      const path = this._selectedNodePaths.shift();
      if(this._treeMap && this._treeMap[path] && this._treeMap[path].selected){
        console.log(this.class, `deselect: ${path} ...`)
        this._treeMap[path].deselect()
      }   
    }
  }

  _collapseTreeNodes = () => {
    console.log(this.class, `_collapseTreeNodes: ${this._openedNodes.length} ...`)
    while(this._openedNodes.length){
      const path = this._openedNodes.shift();
      if(this._treeMap && this._treeMap[path] && this._treeMap[path].opened){
        console.log(this.class, `close: ${path} ...`)
        this._treeMap[path].close()
      }   
    }
  }

  _expandTreeNodes = () => {
    console.log(this.class, `_expandTreeNodes: ${this._treeMap.length} ...`)
    for(var path in this._treeMap){
      console.log(this.class, `_expandTreeNode: ${path} depth: ${this._treeMap[path].depth} ...`)
      //if(!this._treeMap[path].isLeaf)
      this._treeMap[path].open()
    }
  }

  _refreshComponent = () => {
    this.forceUpdate()
  }

  _openBranchUp = (node) => {
      if(!node) return
      if(node.hidden){
        console.log(this.class, `_openBranchUp: [${node.path}] depth: ${node.depth} hidden: ${node.hidden}`) 
        if(node.depth > 1){
          this._openBranchUp(node.parentNode) 
          if(node.parentNode && !node.parentNode.opened){
            console.log(this.class, "node: ", node)
            console.log(this.class, "node.parentNode: ", node.parentNode)
            console.log(this.class, "node.parentNode.open: ", node.parentNode.open)
            node.parentNode.open()  
          }
        } 
      } 
  }

  _openBranchDown = (node) => {
      if(!node) return
      console.log(this.class, `_openBranchDown: [${node.path}] depth: ${node.depth} hidden: ${node.hidden}`) 
      console.warn(node) 
      if(!node.ifLeaf){
        this._openBranchDown(node.childNodes) 
      }else node.open()   
  }

  _findNodeByPath = (pathChunks, arr) => {
    if(!arr) return
    var chunks = pathChunks
    const chunk = chunks.shift();
    var node = {};
    for(var n of arr){
      if(n.name === chunk){
        node = n
        break
      } 
    }
    if(!chunks.length) return node
    else return this._findNodeByPath(chunks, node.childNodes)
  }

  _addNode = (parentPath, name = 'New node') => (evt) => {
    evt.stopPropagation()
    console.log(this.class, `parentPath: `, parentPath) 
     
    if(parentPath){
      const nodeRoute = convertPath2Route(parentPath)
      console.log(this.class, `this.state.data: `, this.state.data) 
      console.log(this.class, `nodeRoute: `, nodeRoute)
      var nodeRouteChunks = nodeRoute.split('.')
      console.log(this.class, `nodeRouteChunks: `, nodeRouteChunks)
      var parentNode = this._findNodeByPath(nodeRouteChunks, this.state.data)
      if(parentNode){
        parentNode.opened = true
        if(!parentNode.childNodes) parentNode.childNodes = []  
        const newNode = {name: (name + ' ' + (nodeRoute.split('.').length) + '-' + (parentNode.childNodes.length))}
        console.log(this.class, "**newNode: ", newNode) 
        parentNode.childNodes.push(newNode)
      }
    }else{
      var treeRoot = this.state.data
      const newNode = {name: (name + ' ' + (Object.keys(this.state.data).length))}
      treeRoot.push(newNode)
    }
    
    console.log(this.class, "**this.state.data: ", this.state.data) 
    this._refreshComponent()
  }
  render() {
    const selectedPath = this._selectedNodePaths && this._selectedNodePaths[0]

    const expandButton = this.openedCount > 0?
          (<button key="toggle-collapse" className="btn btn-light btn-sm mr-1" onClick={this.collapseTree}><Icons.Collapse/></button>):
          (<button key="toggle-collapse" className="btn btn-light btn-sm mr-1" onClick={this.expandTree}><Icons.Expand/></button>)
     const addButton = <button key="add" className="btn btn-warning btn-sm mr-1" onClick={this._addNode(selectedPath)}>
                        <Icons.Plus/>
                      </button>
          
    return (
      <div>
        <TreeHeader className="float-right" title="FlowTree" path={selectedPath}>
          {[addButton, expandButton]}
        </TreeHeader>
        <FlowBranch
          {...this.props}
          childNodes={this.state.data}
          ref={ref => ref && (this._childNodes = ref._childNodes)}
          parentNode={this}
          root={this}
          onSelect={this.handleSelect}
          onDeselect={this.handleDeselect}
          onOpen={this.handleOpen}
          onClose={this.handleClose}
          onCreate={(node) => this._treeMap[node.path] = node}
        />
        
      </div>
    )
  }
}

export default FlowTree
