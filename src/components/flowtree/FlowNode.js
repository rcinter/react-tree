import PropTypes from 'prop-types'
import React from 'react'

import Icons from './icons'
import { exports } from './modules'
import Shapes from './shapes'

class FlowNode extends React.Component {
  static propTypes = {
    node: Shapes.Node,
    depth: PropTypes.number,
    hidden: PropTypes.bool,
    branch: PropTypes.instanceOf(React.Component).isRequired,
    parentNode: PropTypes.instanceOf(React.Component).isRequired,
    root: PropTypes.instanceOf(React.Component).isRequired,
    onSelect: PropTypes.func,
    onDeselect: PropTypes.func,
    onClose: PropTypes.func,
    onOpen: PropTypes.func,
    onCreate: PropTypes.func,
  }

  static defaultProps = {
    depth: 0,
    hidden: true,
    onSelect: () => {},
    onDeselect: () => {},
    onClose: () => {},
    onOpen: () => {},
    onCreate: () => {},
  }

  constructor(props) {
    super(props)

    this._path = props.parentNode._path + '/' + props.node.name
    this._childNodes = []
    this.state = {
      node: props.node
    }
    if (props.hidden)  this._createHiddenChildNodes()  
    this.props.onCreate(this)
  }

  get class() {
    return `(${this.constructor.name} [${this.name}])`
  }

  get depth() {
    return this.props.depth
  }

  get hidden() {
    return this.props.hidden
  }

  get parentNode() {
    return this.props.parentNode
  }

  get root() {
    return this.props.root
  }

  get childNodes() {
    return [...this._childNodes]
  }

  get isLeaf() {
    return !(this.state.node.childNodes && this.state.node.childNodes.length > 0)
  }

  get path() {
    return this._path
  }

  get name() {
    return this.state.node.name
  }

  get opened() {
    return this.state.node.opened
  }

  get selected() {
    return this.state.node.selected
  }

  get type() {
    return this.state.node.linkTo?'link':'node'
  }

  get linkTo() {
    return this.state.node.linkTo
  }

  get json() {
    let childNodesJson = {}
    if(this.childNodes) this.childNodes.forEach(node => {
      childNodesJson[node.name] = node.json
    })
    return {
      type: this.type,
      name: this.name,
      depth: this.depth,
      path: this.path,
      opened: this.opened,
      selected: this.selected,
      hidden: this.hidden,
      childNodes: childNodesJson,
      isLeaf: this.isLeaf,
      linkTo: this.linkTo
    }
  }

  getSnapshotBeforeUpdate() {
    this._childNodes = []
    return null
  }

  componentDidMount() {
    this._mounted = true
  }

  componentDidUpdate() {
    if (!this.state.opened) {
      this._createHiddenChildNodes()
    }
  }

  componentWillUnmount() {
    this._mounted = false
  }

  //** Public */
  select = () => {
    if (this.state.node.selected) return 
    console.log(this.class, `Select path: [${this.path}] hidden: ${this.hidden} ...`)
    
    if (!this._mounted) {
      const node = this.state.node
      node.selected = true
    }else{
      this.setState({
        node: Object.assign(this.state.node, {
          selected: true
        })
      })
    }
    this.props.onSelect(this)
  }
  
  deselect = () => {
    if (!this.state.node.selected) return
    console.log(this.class, `Deselect path: [${this.path}] ...`)
    
    if (!this._mounted) {
      const node = this.state.node
      node.selected = false
    }else{
      this.setState({
        node: Object.assign(this.state.node, {
          selected: false
        })
      })
    }
    this.props.onDeselect(this) 
  }

  open = () => {
    if (this.state.node.opened) return
    console.log(this.class, `Open path: [${this.path}] ...`)
    this.setState({
      node: Object.assign(this.state.node, {
        opened: true
      })
    })  
    this.props.onOpen(this)
  }

  close = () => {
    if (!this.state.node.opened) return
    console.log(this.class, `Close path: [${this.path}] ...`)
    this.setState({
      node: Object.assign(this.state.node, {
        opened: false
      })
    })  
    this.props.onClose(this)
  }

  toggleSelect() {
    return this.state.node.selected ? this.deselect() : this.select()
  }
  toggleOpen() {
    return this.state.node.opened ? this.close() : this.open()
  }

  //** Private */
  _createHiddenChildNodes = () => {
    if (!this.state.node.childNodes) return

    this.state.node.childNodes.forEach((node) => {
      const ref = new FlowNode({
        node,
        hidden: true,
        branch: this.props.branch,
        parentNode: this,
        root: this.props.root,
        depth: this.props.depth + 1,
        onSelect: this.props.onSelect,
        onDeselect: this.props.onDeselect,
        onOpen: this.props.onOpen,
        onClose: this.props.onClose,
        onCreate: this.props.onCreate,
      })

      this._childNodes.push(ref)
    })
  }
  
  _handleOpen = evt => {
    evt.stopPropagation()
    if(!this.isLeaf){
      if(!this.selected){
         this.select()
         this.open()
      }else{
        this.deselect()
        this.close()
      }  
    } else this.toggleSelect()
  }

  _getCssClass = () => {
    switch (this.type) {
      case 'link':
        return "flowNode-link"
      default:
        return "flowNode-node"
    }
  }

  render() {

    const textclass = (this.depth === 1)?"flowNode-script-text":"flowNode-node-text"
    const selectedClass = this.state.node.selected?"list-group-item-success":""
    const nodeIconClass = this.isLeaf?"flowNode-icon pl-3":"flowNode-icon"
    const nodeClass = !this.isLeaf?"flowNode-branch":""
    return (
      <li className={`branch-node-group-item list-group-item pl-3 ${selectedClass}`} onClick={this._handleOpen}>
        <div className={this._getCssClass(this.type)}>
          <div className={`flowNode-descriptor ${nodeClass}`}>
            <div className="flowNode-carret-icon">
                {!this.isLeaf?Icons.getCaret(this.state.node.opened):''}
            </div>
            <div className={nodeIconClass}>
              {Icons.getIcon({ opened: this.state.node.opened, 
                               empty: this.isLeaf,
                               type: this.type,
                               depth: this.props.depth })}
            </div>
            <div className={textclass}>{this.state.node.name}</div>
            {this.type === 'link'?<div className={"flowNode-icon"}>
              <Icons.LongArrow />
              {Icons.getIcon({ opened: false, 
                  empty: true,
                  type: 'node',
                  depth: this.props.depth + 1 })}
              {'   '}{this.linkTo.name}
            </div>:''}
          </div>
          {this.state.node.childNodes && this.state.node.opened && (
            <exports.FlowBranch
              ref={ref => ref && (this._childNodes = ref._childNodes)}
              childNodes={this.state.node.childNodes}
              parentNode={this}
              root={this.props.root}
              depth={this.props.depth}
              onSelect={this.props.onSelect}
              onDeselect={this.props.onDeselect}
              onOpen={this.props.onOpen}
              onClose={this.props.onClose}
              onCreate={this.props.onCreate}
            />
          )}
        </div>
      </li>
    )
  }
}

exports.FlowNode = FlowNode
