import PropTypes from 'prop-types'

const Node = (() => {
  
  const linkToNode = {
    path: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  }
  const shape = {
    name: PropTypes.string,
    opened: PropTypes.boolean,
    selected: PropTypes.boolean,
    linkTo: PropTypes.shape(linkToNode)
  }
  shape.childNodes = PropTypes.arrayOf(PropTypes.shape(shape))
  return PropTypes.shape(shape)
})()

export default {
  Node
}
