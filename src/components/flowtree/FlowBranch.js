
import PropTypes from 'prop-types'
import React from 'react'

import { exports } from './modules'
import Shapes from './shapes'

class FlowBranch extends React.Component {
  static propTypes = {
    childNodes: PropTypes.arrayOf(Shapes.Node),
    parentNode: PropTypes.instanceOf(React.Component).isRequired,
    root: PropTypes.instanceOf(React.Component).isRequired,
    depth: PropTypes.number,
    onSelect: PropTypes.func,
    onDeselect: PropTypes.func,
    onClose: PropTypes.func,
    onOpen: PropTypes.func,
    onCreate: PropTypes.func,
  }

  static defaultProps = {
    depth: 0,
    onSelect: () => {},
    onDeselect: () => {},
    onClose: () => {},
    onOpen: () => {},
  }

  constructor(props) {
    super(props)
    this._path = props.parentNode._path + '/'
    this._childNodes = []
  }

  get class() {
    return `(${this.constructor.name} [${this.name}])`
  }

  get depth() {
    return this.props.depth
  }

  get parentNode() {
    return this.props.parentNode
  }

  get root() {
    return this.props.root
  }

  get childNodes() {
    return [...this._childNodes]
  }

  get path() {
    return this._path
  }

  get json() {
    let childNodesJson = {}
    if(this.childNodes) this.childNodes.forEach(node => {
      childNodesJson[node.name] = node.json
    })
    return {
      depth: this.depth,
      path: this.path,
      childNodes: childNodesJson,
    }
  }

  open = () => {
    if(this.parentNode && !this.parentNode.opened)
      this.parentNode.open()
  }

  getSnapshotBeforeUpdate() {
    this._childNodes = []
    return null
  }
  
  componentDidUpdate() {}

  render() {
    return (
        <ul className="branch-node-group list-group border border-0">
          {this.props.childNodes.map((node, i) => (
              <exports.FlowNode key={`${node.depth}_${node.name}_${i}`} 
                ref={ref => ref && this._childNodes.push(ref)}
                node={node}
                branch={this}
                parentNode={this.props.parentNode}
                root={this.props.root}
                depth={this.props.depth + 1}
                onSelect={this.props.onSelect}
                onDeselect={this.props.onDeselect}
                onClose={this.props.onClose}
                onOpen={this.props.onOpen}
                onCreate={this.props.onCreate}
              />
          ))}
        </ul>
    )
  }
}

exports.FlowBranch = FlowBranch

