import test from './test';
import bot from './bot';
import botSample from './bot-sample';

export default {
    bot: bot,
    sample: botSample,
    test: test,
}