export default [
{
	"_id": {
		"$oid": "5d5517e6b9e9b2b334044990"
	},
	"action_id": "close account",
	"script": "flow",
	"wait_for_input": true,
	"text": [
		"The weather is nice. Ш§Щ„Ш·Щ‚Ші Ш¬Щ…ЩЉЩ„"
	],
	"intent": "close account",
	"delay": "0",
	"button": [],
	"quick": [],
	"intro": false,
	"outro": false
},
{
	"_id": {
		"$oid": "5d5517e6b9e9b2b334044995"
	},
	"action_id": "open account",
	"script": "flow",
	"wait_for_input": true,
	"text": [
		"You can open an account. ЩЉЩ…ЩѓЩ†Щѓ ЩЃШЄШ­ Ш­ШіШ§ШЁ."
	],
	"intent": "open account",
	"delay": "0",
	"button": [],
	"quick": [],
	"intro": false,
	"outro": false
},
{
	"_id": {
		"$oid": "5d5517e6b9e9b2b33404489e"
	},
	"action_id": "when do payments arrive",
	"script": "main",
	"wait_for_input": true,
	"text": [
		"Pleace choose action:\n1. Open account\n2. Close account\n3.Whatever"
	],
	"intent": "when do payments arrive",
	"button": [],
	"quick": [
		"1",
		"2",
        "3"
	],
	"intro": false,
	"outro": false,
	"router": {
		"\\u002e*1": "flow.open account",
		"\\u002e*2": "flow.close account",
        "\\u002e*3": "main.GoodBye",
	}
},
{
	"_id": {
		"$oid": "5d5517e6b9e9b2b334044886"
	},
	"action_id": "GoodBye",
	"script": "main",
	"wait_for_input": false,
	"text": [
		"Bye! рџ‘‹ рџ‘‹ рџ‘‹",
		""
	],
	"intent": "GoodBye",
	"delay": "0",
	"button": [],
	"quick": [],
	"intro": false,
	"outro": false
}]