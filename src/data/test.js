export default 
[
    { name: 'script-0', childNodes: [
        {name: 'main', childNodes: [
            {name: "iterate"}
        ]}
    ]},
    { name: 'script-1', childNodes: [
        { name: '1' },
        { name: '2',
            childNodes: [
                {name: "2.1"},
                {name: "2.2", 
                    childNodes: [
                        {name: "2.2.1"},
                        {name: "2.2.2",
                            childNodes: [
                                {name: "2.2.2.1"},
                                {name: "2.2.2.2"}
                            ]
                        },
                        {name: "2.2.3",
                            childNodes: [
                                {name: "2.2.3.1"},
                                {name: "2.2.3.2"},
                                {name: "2.2.3.3"}
                            ]
                        },
                        {name: "2.2.4",
                            childNodes: [
                                {name: "2.2.4.1"},
                                {name: "2.2.4.2"},
                                {name: "2.2.4.3"},
                                {name: "2.2.4.4"},
                                {name: "2.2.4.5", 
                                    childNodes: [
                                        {name: "2.2.4.5.1"},
                                        {name: "2.2.4.5.2"}
                                    ]
                                },
                                {name: "2.2.4.6"}
                            ]
                        }
                    ]
                },
                {name: "2.3"},
            ]
        },
        { name: '3.1', 
            childNodes: [
                {name: "3.1.1"},
                {name: "3.1.2"},
                {name: "3.1.3",
                    childNodes: [
                        {name: "3.1.3.1"},
                        {name: "3.1.3.2",
                            childNodes: [
                            {name: "3.1.3.2.1",
                                childNodes: [
                                    {name: "3.1.3.2.1.1"},
                                    {name: "3.1.3.2.1.2",
                                    childNodes: [
                                        {name: "3.1.3.2.1.2.1", 
                                            childNodes: [
                                                {name: "route A", linkTo: { path: "~/script-1/1", name: "1"}},
                                                {name: "route B", linkTo: { path: "~/script-1/2/2.1", name: "2.1"}},
                                                {name: "route C", linkTo: { path: "~/script-1/2/2.2/2.2.2/2.2.2.2", name: "2.2.2.2"}},
                                                {name: "route D", linkTo: { path: "~/script-1/2/2.2/2.2.4/2.2.4.5/2.2.4.5.1", name: "2.2.4.5.1"}}
                                            ]
                                        },
                                    ]},
                                ]},
                            ]},
                    ]
                },
            ]
        },
        { name: '3.2', 
            childNodes: [
                {name: "ext. script", linkTo: {path: "~/script-0/main/iterate", name: "iterate"}},
            ]
        },
    ]},
]