
import React from 'react'
import { BrowserRouter as Router, Route, Link, Redirect } from "react-router-dom";
import FlowEditor from './components/floweditor'
import data from './data'
import './App.css'


class App extends React.Component {
  onSelectChange = (nodes) => {
    nodes.forEach(node => {
      if(node.selected) console.log(`%c[${node.path}] selected ...`, 'background: darkgreen; color: yellow')
      else console.log(`%c[${node.path}] deselected ...`, 'background: darkgreen; color: yellow')  
    })
  }
  onOpenClose = (node) => {
    if(node.opened) console.log(`%c[${node.path}] opened ...`, 'background: darkgreen; color: yellow')
    else console.log(`%c[${node.path}] closed ...`, 'background: darkgreen; color: yellow')
  }

  render () {
    
    var linkRoutes = []
    for(var key of Object.keys(data)){
      const to = "/" + key
      linkRoutes.push(<li key={'route-link-' + key} className="nav-item">
                        <Link className="nav-link" to={to}>
                          {key.charAt(0).toUpperCase() + key.slice(1)}
                        </Link>
                      </li>)                                  
    }
    
    var routes = []
    routes.push(<Route key="bot" exact path="/bot" render={(props) => <FlowEditor {...props} onSelectChange={this.onSelectChange}
                                                                    onOpenClose={this.onOpenClose}
                                                                    data={data.bot}
                                                                    mode={'bot'}/>} />)
    routes.push(<Route key="test" exact path="/test" render={(props) => <FlowEditor {...props} onSelectChange={this.onSelectChange}
                                                                        onOpenClose={this.onOpenClose}
                                                                        data={data.test}
                                                                        mode={'test'}/>} />)
    routes.push(<Route key="sample" exact path="/sample" render={(props) => <FlowEditor {...props} onSelectChange={this.onSelectChange}
                                                                        onOpenClose={this.onOpenClose}
                                                                        data={data.sample}
                                                                        mode={'bot'}/>} />  )                                                                    

    return (
      <Router>
        <div className="App container">
          <div className="row">
            <div className="col-12 flowEditorHeader">
                <nav className="navbar navbar-expand-lg navbar-light">
                  <Link className="navbar-brand" to="/">FlowEditor</Link>
                  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                  </button>
                  <div className="collapse navbar-collapse" id="navbarText">
                    <ul className="navbar-nav mr-auto">
                      {linkRoutes}
                    </ul>
                  </div>
                </nav>
            </div>
          </div>
          <Redirect exact from="/" to="/bot" />
          {routes} 
        </div>
      </Router>)
    
    
  }
}


export default App
